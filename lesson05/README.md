# Notes

* The surface should have a bit depth > 8. Paint.NET automatically chooses a bit depth,
  so double check when creating images.