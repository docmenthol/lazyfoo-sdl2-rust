extern crate sdl2;

use sdl2::event::Event;
use sdl2::rect::Rect;
use sdl2::render::{Texture, TextureCreator};
use sdl2::surface::Surface;
use sdl2::video::{Window, WindowContext};
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;

    let window = video
        .window("Lesson 05", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn surface_to_texture<'a>(
    surface: &'a Surface,
    texture_creator: &'a TextureCreator<WindowContext>,
) -> Texture<'a> {
    match texture_creator.create_texture_from_surface(&surface) {
        Ok(image_texture) => image_texture,
        Err(err) => panic!("Failed to create texture from surface. Error: {}", err),
    }
}

fn load_image(path: &'static str) -> Surface {
    use std::path::Path;
    match Surface::load_bmp(&Path::new(path)) {
        Ok(surface) => surface,
        Err(err) => panic!("Could not load texture. Error: {}", err),
    }
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let image_surface = load_image("resources/stretch.bmp");

    let pixel_format = window.window_pixel_format();
    let sf_pixel_format = image_surface.pixel_format();
    let optimized_surface = image_surface.convert(&sf_pixel_format)?;

    let dst_rect = Rect::new(0, 0, WIDTH, HEIGHT);
    let mut stretched_surface = Surface::new(WIDTH, HEIGHT, pixel_format)?;
    optimized_surface.blit_scaled(None, &mut stretched_surface, Some(dst_rect))?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let image_texture = surface_to_texture(&optimized_surface, &texture_creator);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.clear();
        renderer.copy(&image_texture, None, None)?;
        renderer.present();
    }

    Ok(())
}
