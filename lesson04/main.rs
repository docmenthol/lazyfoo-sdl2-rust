extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::{Texture, TextureCreator};
use sdl2::surface::Surface;
use sdl2::video::{Window, WindowContext};
use sdl2::Sdl;
use std::collections::HashMap;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;

    let window = video
        .window("Lesson 04", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn load_image(path: &'static str) -> Surface {
    use std::path::Path;
    match Surface::load_bmp(&Path::new(path)) {
        Ok(surface) => surface,
        Err(err) => panic!("Could not load texture. Error: {}", err),
    }
}

fn load_texture<'a>(
    path: &'static str,
    texture_creator: &'a TextureCreator<WindowContext>,
) -> Texture<'a> {
    let image = load_image(path);
    match texture_creator.create_texture_from_surface(&image) {
        Ok(image_texture) => image_texture,
        Err(err) => panic!("Failed to create texture from surface. Error: {}", err),
    }
}

fn load_media<'a>(
    texture_creator: &'a TextureCreator<WindowContext>,
) -> HashMap<&'static str, Box<Texture>> {
    let mut map: HashMap<&'static str, Box<Texture>> = HashMap::new();
    map.insert(
        "up",
        Box::new(load_texture("resources/up.bmp", &texture_creator)),
    );
    map.insert(
        "down",
        Box::new(load_texture("resources/down.bmp", &texture_creator)),
    );
    map.insert(
        "left",
        Box::new(load_texture("resources/left.bmp", &texture_creator)),
    );
    map.insert(
        "right",
        Box::new(load_texture("resources/right.bmp", &texture_creator)),
    );
    map.insert(
        "press",
        Box::new(load_texture("resources/press.bmp", &texture_creator)),
    );
    map
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;
    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let sprites: HashMap<&'static str, Box<Texture>> = load_media(&texture_creator);

    let mut running: bool = true;
    let mut current_image: &str = "press";

    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) | Some(Keycode::Q) => running = false,
                    Some(Keycode::Up) => current_image = "up",
                    Some(Keycode::Down) => current_image = "down",
                    Some(Keycode::Left) => current_image = "left",
                    Some(Keycode::Right) => current_image = "right",
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.clear();
        renderer.copy(&sprites[current_image], None, None)?;
        renderer.present();
    }

    Ok(())
}
