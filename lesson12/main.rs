extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 12", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut red_tint: u8 = 255;
    let mut green_tint: u8 = 255;
    let mut blue_tint: u8 = 255;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let mut background = LTexture::new_from_file("resources/full.png", &texture_creator);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    Some(Keycode::Q) => {
                        if red_tint > 31 {
                            red_tint -= 32;
                        } else {
                            red_tint = 0;
                        }
                    }
                    Some(Keycode::W) => {
                        if red_tint < 223 {
                            red_tint += 32;
                        } else {
                            red_tint = 255;
                        }
                    }
                    Some(Keycode::A) => {
                        if green_tint > 31 {
                            green_tint -= 32;
                        } else {
                            green_tint = 0;
                        }
                    }
                    Some(Keycode::S) => {
                        if green_tint < 223 {
                            green_tint += 32;
                        } else {
                            green_tint = 255;
                        }
                    }
                    Some(Keycode::Z) => {
                        if blue_tint > 31 {
                            blue_tint -= 32;
                        } else {
                            blue_tint = 0;
                        }
                    }
                    Some(Keycode::X) => {
                        if blue_tint < 223 {
                            blue_tint += 32;
                        } else {
                            blue_tint = 255;
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();

        background.set_color(Color::RGB(red_tint, green_tint, blue_tint));
        background.render_to(&mut renderer, 0, 0, None);

        renderer.present();
    }

    Ok(())
}
