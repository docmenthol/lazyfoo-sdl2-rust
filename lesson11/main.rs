extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 11", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let sprites = LTexture::new_from_file("resources/sprites.png", &texture_creator);

    let clip_rects = [
        Rect::new(0, 0, 100, 100),
        Rect::new(100, 0, 100, 100),
        Rect::new(0, 100, 100, 100),
        Rect::new(100, 100, 100, 100),
    ];

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();

        sprites.render_to(&mut renderer, 0, 0, Some(clip_rects[0]));

        sprites.render_to(
            &mut renderer,
            (WIDTH - clip_rects[1].width() * 2) as i32,
            0,
            Some(clip_rects[1]),
        );

        sprites.render_to(
            &mut renderer,
            0,
            (HEIGHT - clip_rects[2].height() * 2) as i32,
            Some(clip_rects[2]),
        );

        sprites.render_to(
            &mut renderer,
            (WIDTH - clip_rects[3].width() * 2) as i32,
            (HEIGHT - clip_rects[3].height() * 2) as i32,
            Some(clip_rects[3]),
        );

        renderer.present();
    }

    Ok(())
}
