extern crate sdl2;

use sdl2::pixels::Color;
use std::thread::sleep;
use std::time::Duration;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video = sdl_context.video()?;

    let window = video
        .window("Lesson 01", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;

    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();

    sleep(Duration::new(2, 0));

    Ok(())
}
