# lazyfoo-sdl2-rust

![build](https://img.shields.io/gitlab/pipeline/docmenthol/lazyfoo-sdl2-rust/master?style=flat-square)

I'm not a Rust programmer or a knowledgable about SDL2, so I decided to do both at once. A lot of
the existing versions of this are 4-6 years old and use a (much) older version of `rust-sdl2`.

So I'm simultaneously learning while also updating them to a newer version of `rust-sdl2`.

To run an individual lesson, invoke cargo to run a lesson by name:

```sh
$ cargo run --bin lesson01
```

If adding a new lesson, don't forget to add it to `Cargo.toml` so it can be run.