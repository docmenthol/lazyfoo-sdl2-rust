extern crate sdl2;

mod lbutton;
mod ltexture;

use crate::lbutton::LButton;
use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::image::Sdl2ImageContext;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::ttf::Sdl2TtfContext;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window, Sdl2ImageContext, Sdl2TtfContext), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let image = match sdl2::image::init(InitFlag::PNG) {
        Ok(image) => image,
        Err(err) => panic!("Could not initialize sdl22_image. Error: {}", err),
    };
    let ttf = match sdl2::ttf::init() {
        Ok(ttf) => ttf,
        Err(err) => panic!("Could not initialize sdl2_ttf. Error: {}", err),
    };

    let window = video
        .window("Lesson 17", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window, image, ttf))
}

fn main() -> Result<(), String> {
    let (sdl_context, window, _image_context, _ttf_context) = init()?;

    let mut renderer = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    let mut buttons: [LButton; 4] = [
        LButton::new(0, 0, 320, 240, &texture_creator),
        LButton::new(320, 0, 320, 240, &texture_creator),
        LButton::new(0, 240, 320, 240, &texture_creator),
        LButton::new(320, 240, 320, 240, &texture_creator),
    ];

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    _ => {}
                },
                _ => {}
            }
        }

        let mouse = event_pump.mouse_state();
        for b in 0..4 {
            buttons[b].handle_event(&mouse);
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));
        renderer.clear();

        for b in &buttons {
            b.render_to(&mut renderer);
        }

        renderer.present();
    }

    Ok(())
}
