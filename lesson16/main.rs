extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::image::Sdl2ImageContext;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::ttf::Sdl2TtfContext;
use sdl2::video::Window;
use sdl2::Sdl;
use std::path::Path;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window, Sdl2ImageContext, Sdl2TtfContext), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let image = match sdl2::image::init(InitFlag::PNG) {
        Ok(image) => image,
        Err(err) => panic!("Could not initialize sdl22_image. Error: {}", err),
    };
    let ttf = match sdl2::ttf::init() {
        Ok(ttf) => ttf,
        Err(err) => panic!("Could not initialize sdl2_ttf. Error: {}", err),
    };

    let window = video
        .window("Lesson 16", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window, image, ttf))
}

fn main() -> Result<(), String> {
    let (sdl_context, window, _image_context, ttf_context) = init()?;

    let mut renderer = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let font = match ttf_context.load_font(&Path::new("resources/joystix.ttf"), 36) {
        Ok(font) => font,
        Err(err) => panic!("Could not load font. Error: {}", err),
    };

    let text = LTexture::new_from_rendered_text(
        font,
        "Hello, SDL TTF.",
        Color::RGB(0, 0, 0),
        &texture_creator,
    );

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    let mut flip_h = false;
    let mut flip_v = false;
    let mut angle = 0.0;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    Some(Keycode::Q) => flip_h = !flip_h,
                    Some(Keycode::W) => {
                        flip_h = false;
                        flip_v = false;
                    }
                    Some(Keycode::E) => flip_v = !flip_v,
                    Some(Keycode::A) => angle -= 15.0,
                    Some(Keycode::D) => angle += 15.0,
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();
        text.render_to(
            &mut renderer,
            (WIDTH / 2 - text.width / 2) as i32,
            (HEIGHT / 2 - text.height) as i32,
            None,
            None,
            angle,
            flip_h,
            flip_v,
        );

        renderer.present();
    }

    Ok(())
}
