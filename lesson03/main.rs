extern crate sdl2;

use sdl2::event::Event;
use sdl2::render::{Texture, TextureCreator};
use sdl2::surface::Surface;
use sdl2::video::{Window, WindowContext};
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;

    let window = video
        .window("Lesson 03", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn load_image(path: &'static str) -> Surface {
    use std::path::Path;
    match Surface::load_bmp(&Path::new(path)) {
        Ok(surface) => surface,
        Err(err) => panic!("Could not load texture. Error: {}", err),
    }
}

fn load_texture<'a>(
    path: &'static str,
    texture_creator: &'a TextureCreator<WindowContext>,
) -> Texture<'a> {
    let image = load_image(path);
    match texture_creator.create_texture_from_surface(&image) {
        Ok(image_texture) => image_texture,
        Err(err) => panic!("Failed to create texture from surface. Error: {}", err),
    }
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;
    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let image_texture = load_texture("resources/hello_world.bmp", &texture_creator);

    let mut running: bool = true;

    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.clear();
        renderer.copy(&image_texture, None, None)?;
        renderer.present();
    }

    Ok(())
}
