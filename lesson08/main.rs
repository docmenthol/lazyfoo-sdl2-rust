extern crate sdl2;

use sdl2::event::Event;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;

    let window = video
        .window("Lesson 08", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(0xff, 0xff, 0xff));
        renderer.clear();

        let fill_rect = Rect::new(WIDTH as i32 / 4, HEIGHT as i32 / 4, WIDTH / 2, HEIGHT / 2);
        renderer.set_draw_color(Color::RGB(0xff, 0, 0));
        renderer.fill_rect(fill_rect)?;

        let outline_rect = Rect::new(
            WIDTH as i32 / 6,
            HEIGHT as i32 / 6,
            (WIDTH * 2) / 3,
            (HEIGHT * 2) / 3,
        );
        renderer.set_draw_color(Color::RGB(0, 0xff, 0));
        renderer.draw_rect(outline_rect)?;

        renderer.set_draw_color(Color::RGB(0, 0, 0xff));
        renderer.draw_line(
            Point::new(0, HEIGHT as i32 / 2),
            Point::new(WIDTH as i32, HEIGHT as i32 / 2),
        )?;

        renderer.set_draw_color(Color::RGB(0xff, 0xff, 0));
        for i in (0..HEIGHT as i32).step_by(4) {
            renderer.draw_point(Point::new(WIDTH as i32 / 2, i as i32))?;
        }

        renderer.present();
    }

    Ok(())
}
