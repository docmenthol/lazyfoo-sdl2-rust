use crate::LTexture;
use sdl2::mouse::MouseState;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::render::TextureCreator;
use sdl2::video::Window;
use sdl2::video::WindowContext;

pub enum LButtonSprite {
    MouseOut,
    MouseOver,
    MouseDown,
    MouseUp,
}

pub struct LButton<'a> {
    current_sprite: LButtonSprite,
    x: i32,
    y: i32,
    width: u32,
    height: u32,
    pressed: bool,
    texture: LTexture<'a>,
}

impl LButton<'_> {
    pub fn new<'a>(
        x: i32,
        y: i32,
        width: u32,
        height: u32,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> LButton<'a> {
        LButton {
            current_sprite: LButtonSprite::MouseOut,
            x: x,
            y: y,
            width: width,
            height: height,
            pressed: false,
            texture: LTexture::new_from_file("resources/button.png", &texture_creator),
        }
    }

    pub fn handle_event(&mut self, mouse: &MouseState) {
        let (x, y) = (mouse.x(), mouse.y());
        let mut inside = true;
        if (x < self.x)
            || (x > self.x + (self.width as i32))
            || (y < self.y)
            || (y > self.y + (self.height as i32))
        {
            inside = false;
        }
        if !inside {
            self.current_sprite = LButtonSprite::MouseOut;
        } else {
            if mouse.left() {
                self.pressed = true;
                self.current_sprite = LButtonSprite::MouseDown;
            } else {
                if self.pressed {
                    self.pressed = false;
                    self.current_sprite = LButtonSprite::MouseUp;
                } else {
                    self.current_sprite = LButtonSprite::MouseOver;
                }
            }
        }
    }

    pub fn render_to(&self, renderer: &mut Canvas<Window>) {
        let clip_rect = match &self.current_sprite {
            LButtonSprite::MouseOut => Rect::new(0, 0, self.width, self.height),
            LButtonSprite::MouseOver => Rect::new(320, 0, self.width, self.height),
            LButtonSprite::MouseDown => Rect::new(0, 240, self.width, self.height),
            LButtonSprite::MouseUp => Rect::new(320, 240, self.width, self.height),
        };

        self.texture.render_to(
            renderer,
            self.x,
            self.y,
            Some(clip_rect),
            None,
            0.0,
            false,
            false,
        )
    }
}
