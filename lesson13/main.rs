extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 13", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut alpha: u8 = 255;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let background = LTexture::new_from_file("resources/alpha_bg.png", &texture_creator);
    let mut foreground = LTexture::new_from_file("resources/alpha_fg.png", &texture_creator);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    Some(Keycode::W) => {
                        if alpha < 223 {
                            alpha += 32;
                        } else {
                            alpha = 255;
                        }
                    }
                    Some(Keycode::S) => {
                        if alpha > 31 {
                            alpha -= 32;
                        } else {
                            alpha = 0;
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();

        background.render_to(&mut renderer, 0, 0, None);

        foreground.set_alpha(alpha);
        foreground.render_to(&mut renderer, 0, 0, None);

        renderer.present();
    }

    Ok(())
}
