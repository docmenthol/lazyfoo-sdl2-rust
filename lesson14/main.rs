extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 14", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let frames = LTexture::new_from_file("resources/animation.png", &texture_creator);

    let clip_rects = [
        Rect::new(0, 0, 64, 205),
        Rect::new(64, 0, 64, 205),
        Rect::new(128, 0, 64, 205),
        Rect::new(196, 0, 64, 205),
    ];

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    let mut frame = 0;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();
        frames.render_to(
            &mut renderer,
            ((WIDTH / 2) - 32) as i32,
            ((HEIGHT / 2) - 105) as i32,
            Some(clip_rects[frame / 4]),
        );

        renderer.present();

        frame += 1;

        if frame / 4 > 3 {
            frame = 0;
        }
    }

    Ok(())
}
