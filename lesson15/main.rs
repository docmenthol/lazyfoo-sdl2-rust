extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::video::Window;
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 15", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let arrow = LTexture::new_from_file("resources/arrow.png", &texture_creator);

    let center_point = Point::new(100, 100);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    let mut flip_h = false;
    let mut flip_v = false;
    let mut angle = 0.0;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    Some(Keycode::Q) => flip_h = !flip_h,
                    Some(Keycode::W) => {
                        flip_h = false;
                        flip_v = false;
                    }
                    Some(Keycode::E) => flip_v = !flip_v,
                    Some(Keycode::A) => angle -= 15.0,
                    Some(Keycode::D) => angle += 15.0,
                    _ => {}
                },
                _ => {}
            }
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));

        renderer.clear();
        arrow.render_to(
            &mut renderer,
            ((WIDTH / 2) - 100) as i32,
            ((HEIGHT / 2) - 100) as i32,
            None,
            Some(center_point),
            angle,
            flip_h,
            flip_v,
        );

        renderer.present();
    }

    Ok(())
}
