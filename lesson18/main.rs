extern crate sdl2;

mod ltexture;

use crate::ltexture::LTexture;
use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::image::Sdl2ImageContext;
use sdl2::keyboard::{Keycode, Scancode};
use sdl2::pixels::Color;
use sdl2::render::TextureCreator;
use sdl2::ttf::Sdl2TtfContext;
use sdl2::video::Window;
use sdl2::video::WindowContext;
use sdl2::Sdl;
use std::collections::HashMap;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window, Sdl2ImageContext, Sdl2TtfContext), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let image = match sdl2::image::init(InitFlag::PNG) {
        Ok(image) => image,
        Err(err) => panic!("Could not initialize sdl22_image. Error: {}", err),
    };
    let ttf = match sdl2::ttf::init() {
        Ok(ttf) => ttf,
        Err(err) => panic!("Could not initialize sdl2_ttf. Error: {}", err),
    };

    let window = video
        .window("Lesson 18", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window, image, ttf))
}

fn load_media<'a>(
    texture_creator: &'a TextureCreator<WindowContext>,
) -> HashMap<&'static str, Box<LTexture>> {
    let mut map: HashMap<&'static str, Box<LTexture>> = HashMap::new();
    map.insert(
        "up",
        Box::new(LTexture::new_from_file(
            "resources/up.bmp",
            &texture_creator,
        )),
    );
    map.insert(
        "down",
        Box::new(LTexture::new_from_file(
            "resources/down.bmp",
            &texture_creator,
        )),
    );
    map.insert(
        "left",
        Box::new(LTexture::new_from_file(
            "resources/left.bmp",
            &texture_creator,
        )),
    );
    map.insert(
        "right",
        Box::new(LTexture::new_from_file(
            "resources/right.bmp",
            &texture_creator,
        )),
    );
    map.insert(
        "press",
        Box::new(LTexture::new_from_file(
            "resources/press.bmp",
            &texture_creator,
        )),
    );
    map
}

fn main() -> Result<(), String> {
    let (sdl_context, window, _image_context, _ttf_context) = init()?;

    let mut renderer = window
        .into_canvas()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let mut running: bool = true;
    let mut current_image: &str = "press";
    let mut event_pump = sdl_context.event_pump()?;

    let sprites: HashMap<&'static str, Box<LTexture>> = load_media(&texture_creator);

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                Event::KeyDown { keycode: k, .. } => match k {
                    Some(Keycode::Escape) => running = false,
                    _ => {}
                },
                _ => {}
            }
        }

        let keyboard_state = event_pump.keyboard_state();
        if keyboard_state.is_scancode_pressed(Scancode::Up) {
            current_image = "up";
        } else if keyboard_state.is_scancode_pressed(Scancode::Down) {
            current_image = "down";
        } else if keyboard_state.is_scancode_pressed(Scancode::Left) {
            current_image = "left";
        } else if keyboard_state.is_scancode_pressed(Scancode::Right) {
            current_image = "right";
        }

        renderer.set_draw_color(Color::RGB(255, 255, 255));
        renderer.clear();

        &sprites[current_image].render_to(&mut renderer, 0, 0, None, None, 0.0, false, false);

        renderer.present();
    }

    Ok(())
}
