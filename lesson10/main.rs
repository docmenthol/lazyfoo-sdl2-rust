extern crate sdl2;

use sdl2::event::Event;
use sdl2::image::InitFlag;
use sdl2::image::LoadSurface;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::render::WindowCanvas;
use sdl2::render::{Texture, TextureCreator};
use sdl2::surface::Surface;
use sdl2::video::{Window, WindowContext};
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

struct LTexture<'a> {
    texture: Texture<'a>,
    width: u32,
    height: u32,
}

impl LTexture<'_> {
    fn new(texture: Texture) -> LTexture {
        let w = texture.query().width;
        let h = texture.query().height;
        LTexture {
            texture: texture,
            width: w,
            height: h,
        }
    }

    fn new_from_file<'a>(
        path: &'static str,
        texture_creator: &'a TextureCreator<WindowContext>,
        transparent: bool,
    ) -> LTexture<'a> {
        use std::path::Path;
        let mut surface: Surface = match LoadSurface::from_file(&Path::new(path)) {
            Ok(surface) => surface,
            Err(err) => panic!("Could not load surface. Error: {}", err),
        };

        if transparent {
            surface
                .set_color_key(true, Color::RGB(0, 0xff, 0xff))
                .unwrap();
        }

        let texture = match texture_creator.create_texture_from_surface(&surface) {
            Ok(texture) => texture,
            Err(err) => panic!("Could not convert surface to texture. Error: {}", err),
        };

        LTexture::new(texture)
    }

    fn render_to(&self, renderer: &mut WindowCanvas, p: Option<Point>) {
        renderer
            .copy(
                &self.texture,
                None,
                Some(Rect::new(
                    match p {
                        Some(p) => p.x(),
                        _ => 0,
                    },
                    match p {
                        Some(p) => p.y(),
                        _ => 0,
                    },
                    self.width,
                    self.height,
                )),
            )
            .unwrap();
    }
}

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 10", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let background_texture =
        LTexture::new_from_file("resources/background.png", &texture_creator, false);
    let foreground_texture =
        LTexture::new_from_file("resources/foreground.png", &texture_creator, true);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.clear();

        background_texture.render_to(&mut renderer, None);
        foreground_texture.render_to(
            &mut renderer,
            Some(Point::new(
                (WIDTH as i32 / 2) - foreground_texture.width as i32 / 2,
                (HEIGHT as i32 / 2) - foreground_texture.height as i32 / 2,
            )),
        );

        renderer.present();
    }

    Ok(())
}
