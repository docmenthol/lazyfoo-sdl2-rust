extern crate sdl2;

use sdl2::surface::Surface;

use std::path::Path;
use std::thread::sleep;
use std::time::Duration;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video = sdl_context.video()?;

    let window = video
        .window("Lesson 02", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let image_surface = Surface::load_bmp(&Path::new("resources/hello_world.bmp"))?;
    let image_texture = match texture_creator.create_texture_from_surface(&image_surface) {
        Ok(image_texture) => image_texture,
        Err(err) => panic!("Failed to create texture from surface. SDL_Error: {}", err),
    };

    renderer.clear();
    renderer.copy(&image_texture, None, None)?;
    renderer.present();

    sleep(Duration::new(2, 0));

    Ok(())
}
