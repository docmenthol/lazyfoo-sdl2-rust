extern crate sdl2;

use sdl2::event::Event;
use sdl2::image::{InitFlag, LoadTexture};
use sdl2::render::{Texture, TextureCreator};
use sdl2::video::{Window, WindowContext};
use sdl2::Sdl;

const WIDTH: u32 = 640;
const HEIGHT: u32 = 480;

fn init() -> Result<(Sdl, Window), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    sdl2::image::init(InitFlag::PNG)?;

    let window = video
        .window("Lesson 07", WIDTH, HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window))
}

fn load_texture<'a>(
    path: &'static str,
    texture_creator: &'a TextureCreator<WindowContext>,
) -> Texture<'a> {
    use std::path::Path;
    match texture_creator.load_texture(&Path::new(path)) {
        Ok(texture) => texture,
        Err(err) => panic!("Could not load texture. Error: {}", err),
    }
}

fn main() -> Result<(), String> {
    let (sdl_context, window) = init()?;

    let mut renderer = window.into_canvas().build().map_err(|e| e.to_string())?;
    let texture_creator = renderer.texture_creator();

    let image_texture = load_texture("resources/texture.png", &texture_creator);

    let mut running: bool = true;
    let mut event_pump = sdl_context.event_pump()?;

    while running {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => running = false,
                _ => {}
            }
        }

        renderer.clear();
        renderer.copy(&image_texture, None, None)?;
        renderer.present();
    }

    Ok(())
}
