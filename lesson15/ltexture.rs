use sdl2::image::LoadSurface;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;
// use sdl2::render::BlendMode;
// use sdl2::render::WindowCanvas;
use sdl2::render::{Texture, TextureCreator};
use sdl2::surface::Surface;
use sdl2::video::WindowContext;

pub struct LTexture<'a> {
    texture: Texture<'a>,
    pub width: u32,
    pub height: u32,
}

impl LTexture<'_> {
    fn new(texture: Texture) -> LTexture {
        let w = texture.query().width;
        let h = texture.query().height;
        LTexture {
            texture: texture,
            width: w,
            height: h,
        }
    }

    pub fn new_from_file<'a>(
        path: &'static str,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> LTexture<'a> {
        use std::path::Path;
        let mut surface: Surface = match LoadSurface::from_file(&Path::new(path)) {
            Ok(surface) => surface,
            Err(err) => panic!("Could not load surface. Error: {}", err),
        };

        surface
            .set_color_key(true, Color::RGB(0, 0xff, 0xff))
            .unwrap();

        let texture = match texture_creator.create_texture_from_surface(&surface) {
            Ok(texture) => texture,
            Err(err) => panic!("Could not convert surface to texture. Error: {}", err),
        };

        LTexture::new(texture)
    }

    // pub fn set_color(&mut self, color: Color) {
    //     let (r, g, b) = color.rgb();
    //     self.texture.set_color_mod(r, g, b);
    // }

    // pub fn set_alpha(&mut self, alpha: u8) {
    //     self.texture.set_alpha_mod(alpha);
    // }

    // pub fn set_blend_mode(&mut self, blendmode: BlendMode) {
    //     self.texture.set_blend_mode(blendmode);
    // }

    pub fn render_to(
        &self,
        renderer: &mut Canvas<Window>,
        x: i32,
        y: i32,
        clip: Option<Rect>,
        center: Option<Point>,
        angle: f64,
        flip_h: bool,
        flip_v: bool,
    ) {
        let (clip_rect, render_rect) = match clip {
            Some(rect) => (rect, Rect::new(x, y, rect.width(), rect.height())),
            None => (
                Rect::new(0, 0, self.width, self.height),
                Rect::new(x, y, self.width, self.height),
            ),
        };
        let center_point = match center {
            Some(c) => c,
            None => Point::new(0, 0),
        };
        renderer
            .copy_ex(
                &self.texture,
                Some(clip_rect),
                Some(render_rect),
                angle,
                center_point,
                flip_h,
                flip_v,
            )
            .unwrap();
    }
}
